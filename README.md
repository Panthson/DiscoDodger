Disco Dodger is a project I decided to take up because I wanted to get into game
development, and developing a mobile game on your own is much more reasonable
than a full fledged PC game. The game can be found [here](https://play.google.com/store/apps/details?id=com.Panthson.Disco_Dodger).