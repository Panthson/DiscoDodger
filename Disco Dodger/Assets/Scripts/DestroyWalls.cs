﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWalls : MonoBehaviour {
    private void OnTriggerEnter(Collider wall)
    {
        if(wall.gameObject.tag == "Wall")
        {
            Destroy(wall.gameObject);
        }
    }
}
