﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public Transform player;
    public Text score;
    public Text lives;

    private void Start()
    {
        score.text = "0";
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag != "Wall Plane")
            return;

        if(lives.text == "GAME OVER!")
            return;
        //Increment score
        int newScore = int.Parse(score.text) + 1;
        score.text = newScore + "";
    }
}
