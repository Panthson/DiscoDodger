﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class JumpTo : IndividualPlatform, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        switch (this.gameObject.name)
        {
            case "Q Button":
                moveToButtonPress("Q");
                break;
            case "W Button":
                moveToButtonPress("W");
                break;
            case "E Button":
                moveToButtonPress("E");
                break;
            case "R Button":
                moveToButtonPress("R");
                break;
        }
    }

}