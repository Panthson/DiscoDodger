﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RestartGame : MonoBehaviour {
    public Text livesText;
    public Text score;
    public Button restartButton;
    public Transform player;

    public void restart()
    {
        score.text = "0";
        livesText.text = "Lives: XXX";
        player.position = new Vector3(player.position.x, player.position.y, player.position.z - 15);
        GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");
        float rowToDelete = walls[0].GetComponent<Transform>().position.z;
        foreach (GameObject wall in walls)
        {
            Transform wallT = wall.GetComponent<Transform>();
            if (wallT.position.z == rowToDelete)
                Destroy(wall);
            else
                break;
        }

        restartButton.GetComponentInChildren<Text>().text = "";
        restartButton.interactable = false;
    }
}
