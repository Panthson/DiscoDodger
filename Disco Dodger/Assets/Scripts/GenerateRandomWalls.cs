﻿using UnityEngine;
using System.Collections.Generic;

public class GenerateRandomWalls : MonoBehaviour
{
    public GameObject wall;
    public GameObject wallPlane;
    public Transform player;

    private const float wallOffset = 4.5f;

    // Use this for initialization
    void Start()
    {
        createRow(0);
        createRow(wallOffset);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Wall Plane")
        {
            Destroy(collision.gameObject);
            createRow((int)player.position.z + wallOffset);
        }
    }

    void createRow(float row)
    {
        int position;
        int[] wallArray = new int[4] { 0, 1, 2, 3 };
        List<int> walls = new List<int>(wallArray);

        GameObject newPlane;
        /* Generate one random number
         * That number corresponds to a platform
         * DON'T create a platform for that number.
         */
        position = Random.Range(0, walls.Count);
        if (walls[position] != 0) //Q
            Instantiate(this.wall, new Vector3(-2.25f, 2.75f, row + 5.5f), Quaternion.identity);
        if (walls[position] != 1) //W
            Instantiate(this.wall, new Vector3(-.75f, 2.75f, row + 5.5f), Quaternion.identity);
        if (walls[position] != 2) //E
            Instantiate(this.wall, new Vector3(.75f, 2.75f, row + 5.5f), Quaternion.identity);
        if (walls[position] != 3) //R
            Instantiate(this.wall, new Vector3(2.25f, 2.75f, row + 5.5f), Quaternion.identity);

        newPlane = Instantiate(this.wallPlane, new Vector3(0, 2.25f, row /** distance*/ + 5.5f), Quaternion.identity);
        newPlane.transform.Rotate(-90, 0, 0);

    }
}
