﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class IndividualPlatform : MonoBehaviour
{
    private static List<KeyCode> keys;
    private int lives = 3;
    private static bool grounded = true;
    private static Transform activePlatform;
    private const float right1 = 4.64587092f; //acc 100 3.09724728f;
    private const float left1 = 4.64587092f; // acc 100 -3.09724728f;
    private Button restartButton;

    protected float time;

    public Transform player;
    public Transform qPlatform;
    public Transform wPlatform;
    public Transform ePlatform;
    public Transform rPlatform;
    public Animator anim;
    public AudioSource audioBlip;

    private void OnTriggerEnter(Collider collision)
    {
        //Detect if collided with a wall
        if (collision.gameObject.tag == "Wall")
        {
            //Destroy the wall
            Destroy(collision.gameObject);

            Text livesText = GameObject.FindGameObjectWithTag("Lives").gameObject.GetComponent<Text>();
            //Decrement lives
            if (livesText.text.Length > 7 && livesText.text != "GAME OVER!")
            {
                Debug.Log(livesText.text.Length);
                livesText.text = livesText.text.Substring(0, livesText.text.Length - 1);
                lives--;
            }

            //Check if game over
            if (livesText.text == "Lives: ")
                gameOver(livesText);
        }

        if (collision.gameObject.tag == "Platform")
        {
            grounded = true;

            //Position correction
            player.position = new Vector3(activePlatform.position.x, 1.5f, activePlatform.position.z);
        }
    }

    private void Start()
    {
        audioBlip = GetComponent<AudioSource>();

        keys = new List<KeyCode>();
        keys.Add(KeyCode.Q);
        keys.Add(KeyCode.W);
        keys.Add(KeyCode.E);
        keys.Add(KeyCode.R);

        activePlatform = wPlatform;
    }

    public void moveToButtonPress(string key)
    {
        if (!grounded)
            return;

        grounded = false;
        audioBlip.Play();

        float playerPos = player.position.x;
        float qpos = qPlatform.position.x;
        float wpos = wPlatform.position.x;
        float epos = ePlatform.position.x;
        float rpos = rPlatform.position.x;
        float goTo = 0;
        switch (key)
        {
            case "Q":
                goTo = qpos;
                activePlatform = qPlatform;
                break;

            case "W":
                goTo = wpos;
                activePlatform = wPlatform;
                break;

            case "E":
                goTo = epos;
                activePlatform = ePlatform;
                break;

            case "R":
                goTo = rpos;
                activePlatform = rPlatform;
                break;

        }

        float diff = (float)System.Math.Round((goTo - playerPos), 2);
        //Move along the arc
        if (diff == 0)
            this.jump(0);
        else if (diff == 1.5f)
            this.jump(right1);
        else if (diff == 3f)
            this.jump(right1 * 2);
        else if (diff == 4.5f)
            this.jump(right1 * 3);
        else if (diff == -1.5f)
            this.jump(-right1);
        else if (diff == -3f)
            this.jump(-right1 * 2);
        else if (diff == -4.5f)
            this.jump(-right1 * 3);

        //Animate rotation
        if (diff > 0)
            anim.Play("Right");
        else if (diff < 0)
            anim.Play("Left");
        else
            anim.Play("Forward");
    }

    private void jump(float direction)
    {
        player.GetComponent<Rigidbody>().velocity = new Vector3(direction, 25f, 0f);
    }

    private void gameOver(Text livesText)
    {
        livesText.text = "GAME OVER!";

        restartButton = GameObject.FindGameObjectWithTag("Restart Button").gameObject.GetComponent<Button>();
        restartButton.interactable = true;
        restartButton.GetComponentInChildren<Text>().text = "Retry?";
    }

}
