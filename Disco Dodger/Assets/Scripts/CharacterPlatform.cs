﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterPlatform : MonoBehaviour {
    // Update is called once per frame
    public GameObject playerCamera;
    public float speed;

    private bool active = false;

    private Vector3 cameraStart;
    private Vector3 cameraEnd = new Vector3(0, 6.1f, -6.05f);
    private float lerpTime = 1;
    private float currentLerpTime = 0;
    private Button button;
    
    void FixedUpdate()
    {
        if (active)
        {
            transform.Translate(0, 0, .1f);

            currentLerpTime += Time.deltaTime;
            if (currentLerpTime >= lerpTime)
            {
                return;
            }
            float perc = currentLerpTime / lerpTime;
            playerCamera.transform.localPosition = Vector3.Lerp(cameraStart, cameraEnd, perc);

        }

    }

    public void startGame()
    {
        active = true;
        button.interactable = false;
        button.GetComponentInChildren<Text>().text = "";

        Physics.gravity = new Vector3(0, -150.0F, 0);
    }

    private void Start()
    {
        cameraStart = playerCamera.transform.localPosition;
        button = GameObject.FindGameObjectWithTag("Main Button").gameObject.GetComponent<Button>();
    }
}
